
# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.1] - 2020-06-13
### hotfix
- change nord_vpn.conf to nord_vpn

## [0.2.0] - 2020-06-13
- Proper installation

## [0.1.0]
### add
- Project's base

