#!/bin/bash


file_vpn="vpnbook-pl226-udp25000.ovpn"

echo "Désinstallation"

# Make sure only root can run our script
if [ "$(id -u)" == "0" ];
then
	
	sudo rm /usr/bin/nordvpn_start.sh
	sudo rm /usr/bin/nordvpn_uninstall.sh
	sudo rm -rf /etc/openvpn/ovpn_udp
	sudo rm -rf /etc/openvpn/ovpn_tcp

	sudo rm -f /etc/sudoers.d/nord_vpn
	echo "Désinstallé"
	exit 0
fi

echo "Executez ce script en tant que root !"
exit 1


