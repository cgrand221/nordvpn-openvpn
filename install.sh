#!/bin/bash


echo "Installation"

# Make sure only root can run our script
if [ "$(id -u)" == "0" ];
then
	sudo apt-get update
	sudo apt-get install -y net-tools openvpn wget ca-certificates
	if [ ! -f ovpn.zip ]
	then
		sudo -u "$SUDO_USER" curl -L -o ovpn.zip https://downloads.nordcdn.com/configs/archives/servers/ovpn.zip
	fi

	sudo cp -f nordvpn_start.sh /usr/bin/nordvpn_start.sh
	sudo cp -f nordvpn_uninstall.sh /usr/bin/nordvpn_uninstall.sh
	
	sudo chmod 555 /usr/bin/nordvpn_start.sh
	sudo chmod 500 /usr/bin/nordvpn_uninstall.sh

	sudo unzip ovpn.zip -d /etc/openvpn

	sudo chmod u-x+rwX,g-xw+rX,o-xw+rX -R /etc/openvpn/ovpn_tcp
 	sudo chmod u-x+rwX,g-xw+rX,o-xw+rX -R /etc/openvpn/ovpn_udp

	echo 'ALL ALL=(root) NOPASSWD: /usr/bin/nordvpn_start.sh' > /etc/sudoers.d/nord_vpn
	sudo chmod 0440 /etc/sudoers.d/nord_vpn
	echo "installé"
	exit 0
fi

echo "Executez ce script en tant que root !"
exit 1


