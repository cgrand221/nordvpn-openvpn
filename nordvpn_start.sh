#!/bin/bash

set -e

dir1="/etc/openvpn/ovpn_tcp"
dir2="/etc/openvpn/ovpn_udp"
default_vpn="/etc/openvpn/ovpn_udp/no174.nordvpn.com.udp.ovpn"


# Make sure only root can run our script
if [ "$(id -u)" == "0" ];
then
        fichier=""
        pos="0"
        racine="/etc/openvpn/"
        while [ "$fichier" != "$racine" ] && ( [ ! -f "${fichier}" ] || [ "$pos" == "0" ] )
        do
                echo "vpn a utiliser [$default_vpn] "  
                fichier=`sudo -u "$SUDO_USER" bash -c "read -i $racine -e fichier; echo \\$fichier"`
                if [ -f "${fichier}" ]
                then
                        fichier="$(realpath ${fichier})"
                else
                        fichier=""
                fi
                if [[ $fichier =~ $dir1 ]]
                then
                        pos="1"
                else
                        if [[ $fichier =~ $dir2 ]]
                        then
                                pos="1"
                        fi
                fi
        done
        if [ "$fichier" == "$racine" ]
        then
                fichier=$default_vpn
        fi
        
        echo "tapez sur entrée pour demarrer le vpn"
        echo "Faites un ctrl+c pour stopper la connexion vpn"
	read
	echo "demarrage du vpn"
	sudo openvpn --config "$fichier"

	echo "fin de connexion vpn"
	exit 0
fi

echo "Executez ce script en tant que root !"
exit 1
