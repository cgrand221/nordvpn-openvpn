nordvpn-openvpn
===============

* Permet de se connecter au VPN Nord-Vpn en utilisant le logiciel libre OpenVPN avec des fichiers au format .ovpn
* Permet de prémunir contre les fuites vpn grace au firewall ufw
* Ce firewall permet d'avoir un killswitch permettant de bloquer votre connexion réelle et de ne passer que par le vpn
* La liste des vpn disponibles est téléchargeable à l'adresse suivante : https://downloads.nordcdn.com/configs/archives/servers/ovpn.zip (source https://nordvpn.com/fr/tutorials/linux/openvpn/)
* Fonctionnel sous ubuntu

Installation:
-------------
```
git clone git@gitlab.com:cgrand221/nordvpn-openvpn.git
cd nordvpn-openvpn
sudo install.sh
```

Utilisation (aucun mot de passe admin requis grace à l'installation):
-----------------------------------------------------------------------
```
sudo nordvpn_start.sh
```
* Entrez ensuite un fichier .ovpn d'un des répertoires suivant :
	* /etc/openvpn/ovpn_tcp/
	* /etc/openvpn/ovpn_udp/
* Entrez ensuite votre nom d'utilisateur et votre mot de passe
* Vous voilà connecté au VPN
* Il est conseillé d'utiliser le vpn avec une autre session utilisateur de votre pc :
	* Ainsi la plupart des logiciels que vous utiliserai  ne seront pas liés à une identié existante en ligne 
	(attention au cookies, aux navigateurs associés à une identité, aux logiciels avec connection automatique...)

* Il est conseillé également d'installer des extensions de blocage sur votre navigateur pour éviter une fuite du vpn grace aux fonctionnalités de javascript, de webRTC ... (ublock-origin, SafeScript)

* Testez votre nouvelle IP :
	* https://www.whatismyip.com/

* Testez les fuites de votre VPN à l'aide de :
	* https://www.dnsleaktest.com/
	* https://www.whatismyip.com/
	* https://ipleak.net/
	* https://nordvpn.com/

* Pour terminer la connexion au vpn faites un ctrl+c

Désinstallation :
-------------------
```
sudo nordvpn_uninstall.sh
```

